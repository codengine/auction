from auction.handlers.auction_handlers import AuctionHander

class TestBidCreation(TestCase):
    def setUp(self):
        pass

    def test_bid_successfully_created(self):
        bidder_name = "Bidder 1"
        bidder_weight = 23
        bid_amount = 120
        keyword = "My eBook"
        bid_currency = "USD"

        created_status = AuctionHander.add_new_bid(bidder_name=bidder_name,
                                                   bidder_weight=bidder_weight,
                                                   bid_amount=bid_amount,
                                                   keyword=keyword,
                                                   bid_currency=bid_currency)

        assert (created_status == 201)

    def test_bid_exists_for_bidder(self):
        bidder_name = "Bidder 1"
        bidder_weight = 23
        bid_amount = 120
        keyword = "My eBook"
        bid_currency = "USD"

        created_status = AuctionHander.add_new_bid(bidder_name=bidder_name,
                                                   bidder_weight=bidder_weight,
                                                   bid_amount=bid_amount,
                                                   keyword=keyword,
                                                   bid_currency=bid_currency)
        created_status = AuctionHander.add_new_bid(bidder_name=bidder_name,
                                                   bidder_weight=bidder_weight,
                                                   bid_amount=bid_amount,
                                                   keyword=keyword,
                                                   bid_currency=bid_currency)
        assert (created_status == 200)

    def test_bid_creation_failed(self):
        bidder_name = "Bidder 1"
        bidder_weight = 23
        bid_amount = 120
        keyword = "My eBook"
        bid_currency = "USD"

        created_status = AuctionHander.add_new_bid(bidder_name=None,
                                                   bidder_weight=bidder_weight,
                                                   bid_amount=bid_amount,
                                                   keyword=keyword,
                                                   bid_currency=bid_currency)
        assert (created_status == 500)
        created_status = AuctionHander.add_new_bid(bidder_name=bidder_name,
                                                   bidder_weight="122a",
                                                   bid_amount=bid_amount,
                                                   keyword=keyword,
                                                   bid_currency=bid_currency)
        assert (created_status == 500)
        created_status = AuctionHander.add_new_bid(bidder_name=bidder_name,
                                                   bidder_weight=None,
                                                   bid_amount=bid_amount,
                                                   keyword=keyword,
                                                   bid_currency=bid_currency)
        assert (created_status == 500)
        created_status = AuctionHander.add_new_bid(bidder_name=bidder_name,
                                                   bidder_weight=bidder_weight,
                                                   bid_amount=None,
                                                   keyword=keyword,
                                                   bid_currency=bid_currency)
        assert (created_status == 500)
        created_status = AuctionHander.add_new_bid(bidder_name=bidder_name,
                                                   bidder_weight=bidder_weight,
                                                   bid_amount=bid_amount,
                                                   keyword=None,
                                                   bid_currency=bid_currency)
        assert (created_status == 500)

    def tearDown(self):
        pass
