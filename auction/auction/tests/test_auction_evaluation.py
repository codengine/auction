from auction.handlers.auction_evaluator import AuctionEvaluator
from auction.handlers.auction_handlers import AuctionHander


class TestAuctionEvaluation(TestCase):
    def setUp(self):
        pass

    def test_auction_evaluation_found_by_bid(self):
        bidder_name = "Bidder 1"
        bidder_weight = 23
        bid_amount = 120
        keyword = "My eBook"
        bid_currency = "USD"

        created_status = AuctionHander.add_new_bid(bidder_name="Name 1",
                                                   bidder_weight=23,
                                                   bid_amount=100,
                                                   keyword=keyword,
                                                   bid_currency=bid_currency)

        created_status = AuctionHander.add_new_bid(bidder_name="Name 2",
                                                   bidder_weight=20,
                                                   bid_amount=200,
                                                   keyword=keyword,
                                                   bid_currency=bid_currency)

        a = AuctionEvaluator.evaluate_by_bid(keyword=keyword)

        a = a.first()

        assert (a is not None and a.bidder.name == "Name 2")

    def test_auction_evaluation_found_by_revenue(self):
        bidder_name = "Bidder 1"
        bidder_weight = 23
        bid_amount = 120
        keyword = "My eBook"
        bid_currency = "USD"

        created_status = AuctionHander.add_new_bid(bidder_name="Name 1",
                                                   bidder_weight=23,
                                                   bid_amount=100,
                                                   keyword=keyword,
                                                   bid_currency=bid_currency)

        created_status = AuctionHander.add_new_bid(bidder_name="Name 2",
                                                   bidder_weight=20,
                                                   bid_amount=200,
                                                   keyword=keyword,
                                                   bid_currency=bid_currency)

        a = AuctionEvaluator.evaluate_by_revenue(keyword=keyword)

        a = a.first()

        assert (a is not None and a.bidder.name == "Name 1")

    def tearDown(self):
        pass