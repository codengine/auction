from django.urls import reverse
from auction.constants.auction_types import AuctionType
from auction.models.auction import Auction
from auction.models.auction_bid import AuctionBid
from core.views.base_template_view import BaseTemplateView


class HomeView(BaseTemplateView):
    template_name = "home.html"

    def make_verbose(self, value):
        return " ".join(value.split("_")).capitalize()

    def get_auction_type_options(self):
        return [
            (AuctionType.RANK_BY_BID.value, self.make_verbose(AuctionType.RANK_BY_BID.value), True),
            (AuctionType.RANK_BY_REVENUE.value, self.make_verbose(AuctionType.RANK_BY_REVENUE.value), False)
        ]

    def get_context_data(self, **kwargs):
        context = super(HomeView, self).get_context_data(**kwargs)
        context["page_title"] = "Auction Management System"
        context["auction_types"] = self.get_auction_type_options()

        context["auction_create_url"] = reverse("ajax_auction_create")

        context["bids"] = AuctionBid.objects.all()

        return context