from django.http import HttpResponse
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from decimal import Decimal
from django.http import JsonResponse
from auction.constants.auction_created_status import AuctionCreatedStatus
from auction.constants.auction_types import AuctionType
from auction.handlers.auction_evaluator import AuctionEvaluator
from auction.handlers.auction_handlers import AuctionHander
from core.views.base_template_view import BaseTemplateView


@method_decorator(csrf_exempt, name='dispatch')
class AuctionAjaxView(BaseTemplateView):

    # @method_decorator(csrf_exempt)
    def post(self, request, *args, **kwargs):
        response = {
            "status": "FAILED",
            "message": "Unsuccessful",
            "code": AuctionCreatedStatus.AUCTION_CREATION_FAILED.value
        }
        if request.is_ajax():
            bidder_name = request.POST.get("bidder_name")
            bidder_weight = request.POST.get("bidder_weight")
            bid_amount = request.POST.get("bid_amount")
            bid_currency = request.POST.get("bid_currency")
            keyword = request.POST.get("keyword")

            if any([not bidder_name, not bidder_weight, not bid_amount, not bid_currency, not keyword]):
                response["code"] = AuctionCreatedStatus.MISSING_DATA.value
                return JsonResponse(response)

            try:
                bidder_weight = Decimal(bidder_weight)
                bid_amount = Decimal(bid_amount)
            except:
                response["code"] = AuctionCreatedStatus.INVALID_DATA.value
                return JsonResponse(response)

            new_bid_added_status = AuctionHander.add_new_bid(bidder_name=bidder_name,
                                                      bidder_weight=bidder_weight,
                                                      bid_amount=bid_amount,
                                                      keyword=keyword,
                                                      bid_currency=bid_currency)
            response["code"] = new_bid_added_status
            if new_bid_added_status == AuctionCreatedStatus.NEW_AUCTION_CREATED.value:
                response["status"] = "SUCCESS"
                response["message"] = "Successful"
            response["code"] = new_bid_added_status
            return JsonResponse(response)
        else:
            response["message"] = "Invalid Request"
            return JsonResponse(response)


class AuctionEvaluateAjaxView(BaseTemplateView):

    def get(self, request, *args, **kwargs):
        response = {
            "status": "FAILED",
            "message": "Unsuccessful",
            "winner_name": ""
        }
        if request.is_ajax():
            print(request.GET)
            keyword = request.GET.get("keyword")
            auction_type = request.GET.get("atype")
            if auction_type == AuctionType.RANK_BY_BID.value:
                auction_winners = AuctionEvaluator.evaluate_by_bid(keyword=keyword)
                if auction_winners.exists():
                    response["status"] = "SUCCESS"
                    response["message"] = "Successful"
                    response["winner_name"] = auction_winners.first().bidder.name
                    return JsonResponse(response)
                else:
                    response["message"] = "NO_DATA"
                    return JsonResponse(response)
            elif auction_type == AuctionType.RANK_BY_REVENUE.value:
                auction_winners = AuctionEvaluator.evaluate_by_revenue(keyword=keyword)
                if auction_winners.exists():
                    response["status"] = "SUCCESS"
                    response["message"] = "Successful"
                    response["winner_name"] = auction_winners.first().bidder.name
                    return JsonResponse(response)
                else:
                    response["message"] = "NO_DATA"
                    return JsonResponse(response)
            else:
                response["message"] = "Invalid Request"
                return JsonResponse(response)
        else:
            response["message"] = "Invalid Request"
            return JsonResponse(response)