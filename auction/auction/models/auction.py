from django.db import models
from auction.models.auction_bid import AuctionBid
from core.models.base_entity import BaseEntity


class Auction(BaseEntity):
    name = models.CharField(max_length=500)
    bids = models.ManyToManyField(AuctionBid)