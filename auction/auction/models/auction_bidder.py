from django.db import models
from core.models.base_entity import BaseEntity


class AuctionBidder(BaseEntity):
    name = models.CharField(max_length=500)
    weight = models.DecimalField(decimal_places=2, max_digits=20, null=True)