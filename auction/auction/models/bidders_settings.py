from django.db import models

from auction.constants.enums import BidderLimitMode
from core.models.base_entity import BaseEntity


class BiddersSetting(BaseEntity):
    max_limit = models.DecimalField(decimal_places=2, max_digits=20)
    limit_mode = models.CharField(max_length=200, default=BidderLimitMode.WEEKLY.value)
    currency = models.CharField(max_length=20, default="USD")