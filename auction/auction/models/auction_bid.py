from django.db import models
from auction.models.auction_bidder import AuctionBidder
from core.models.base_entity import BaseEntity


class AuctionBid(BaseEntity):
    bidder = models.ForeignKey(AuctionBidder, on_delete=models.CASCADE)
    bid_amount = models.DecimalField(decimal_places=2, max_digits=20)
    bid_currency = models.CharField(max_length=20, default="USD")
    keyword = models.CharField(max_length=500)