from django.db.models import F

from auction.models.auction_bid import AuctionBid


class AuctionEvaluator(object):

    @classmethod
    def evaluate_by_bid(cls, keyword):
        action_bids = AuctionBid.objects.filter(keyword=keyword)
        return action_bids.order_by("-bid_amount")

    @classmethod
    def evaluate_by_revenue(cls, keyword):
        action_bids = AuctionBid.objects.filter(keyword=keyword).annotate(score=F('bidder__weight')*F('bid_amount'))
        return action_bids.order_by("-score")