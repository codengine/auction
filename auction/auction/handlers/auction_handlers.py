from django.db import transaction

from auction.constants.auction_created_status import AuctionCreatedStatus
from auction.models.auction_bid import AuctionBid
from auction.models.auction_bidder import AuctionBidder


class AuctionHander(object):

    """
        This method checks if already auction bid exists for this use. If so then it
        updates otherwise it creates a new bid.
    """
    @classmethod
    def add_new_bid(cls, bidder_name, bidder_weight, bid_amount, keyword, bid_currency):
        try:
            with transaction.atomic():
                bid_exists = False
                auction_bidders = AuctionBidder.objects.filter(name=bidder_name)
                if auction_bidders.exists():
                    auction_bidder = auction_bidders.first()
                    aution_bids = AuctionBid.objects.filter(bidder_id=auction_bidder.pk)
                    if aution_bids.exists():
                        bid_exists = True

                if not bid_exists:
                    if auction_bidders.exists():
                        auction_bidder = auction_bidders.first()
                    else:
                        auction_bidder = AuctionBidder()
                        auction_bidder.name = bidder_name
                        auction_bidder.weight = bidder_weight
                        auction_bidder.save()

                    auction_bid = AuctionBid()
                    auction_bid.bidder_id = auction_bidder.pk
                    auction_bid.bid_amount = bid_amount
                    auction_bid.bid_currency = bid_currency
                    auction_bid.keyword = keyword
                    auction_bid.save()

                    return AuctionCreatedStatus.NEW_AUCTION_CREATED.value
            return AuctionCreatedStatus.AUCTION_EXISTS.value
        except Exception as exp:
            # Log the stacktrace here.
            return AuctionCreatedStatus.AUCTION_CREATION_FAILED.value
