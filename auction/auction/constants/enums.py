from enum import Enum


class BidderLimitMode(Enum):
    WEEKLY = "by_weekly"