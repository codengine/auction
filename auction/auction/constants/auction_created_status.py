from enum import Enum


class AuctionCreatedStatus(Enum):
    NEW_AUCTION_CREATED = 201
    AUCTION_EXISTS = 202
    AUCTION_CREATION_FAILED = 500
    MISSING_DATA = 400
    INVALID_DATA = 401