from enum import Enum


class AuctionType(Enum):
    RANK_BY_REVENUE = "rank_by_revenue"
    RANK_BY_BID = "rank_by_bid"