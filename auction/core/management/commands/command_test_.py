from django.core.management.base import BaseCommand
from auction.handlers.auction_evaluator import AuctionEvaluator


class Command(BaseCommand):

    def handle(self, *args, **options):
        a = AuctionEvaluator.evaluate_by_bid(keyword="asd")
        print(a)
        a = AuctionEvaluator.evaluate_by_revenue(keyword="asd")
        print(a)